import requests
import json

url = 'http://127.0.0.1:8000/api/quotes'

all_passed = []


def _did_pass(test: bool, req=None):
    all_passed.append(test)
    if test:
        print('passed!\n')
    else:
        print('failed!\n')
        if req is not None:
            try:
                print(req.json(), req.status_code)
            except:
                print(req.status_code)
        exit()


print('TESTING WITH ANONYMOUS USER\n\n')

print('sending GET')
get = requests.get(url)
_did_pass(get.status_code == 200)

print('sending POST, expecting 401 error')
post = requests.post(url)
_did_pass(post.status_code == 401)

print('sending PATCH, expecting 401 error')
patch = requests.patch(url)
_did_pass(patch.status_code == 401)

print('sending DELETE, expecting 401 error')
delete = requests.delete(url)
_did_pass(delete.status_code == 401)

print('logging in with wrong credentials, expecting error')
headers = {'Content-Type': 'application/json'}
unauthorized = requests.post(url='http://localhost:8000/auth/token/',
                             data=json.dumps({"username": "davidattenborough", "password": "boatymcboatface"}),
                             headers=headers)
_did_pass(unauthorized.status_code == 401)

print('logging in with right credentials')
granted = requests.post(url='http://localhost:8000/auth/token/',
                        data=json.dumps({"username": "meerkat", "password": "123456"}),
                        headers=headers)

_did_pass(granted.status_code == 200)

print('retrieving tokens')
token = granted.json()
_did_pass(all([key in ['refresh', 'access'] for key in token.keys()]))

print('TESTING WITH AUTHENTICATED USER\n\n')
headers['Authorization'] = 'Bearer ' + token['access']

print('sending GET')
get = requests.get(url)
_did_pass(get.status_code == 200)

print('attempting to create a quote')
req = requests.post(url=url,
                    data=json.dumps({"name": "big sale",
                                     "price": 4200.12,
                                     "items": ['mercedes', 'audi', 'bmw']}),
                    headers=headers)
_did_pass(210 > req.status_code >= 200, req)


print('sending GET to check presence')
get = requests.get(url)

try:
    big_sale = [quote for quote in get.json() if 'name' in quote.keys()][0]
    items = [item['name'] for item in big_sale['items']]
    are_items_in = [item in items for item in ['mercedes', 'audi', 'bmw']]
    _did_pass(get.status_code == 200 and big_sale["price"] == '4200.12' and len(items) == 3 and all(are_items_in), get)
except Exception as e:
    print(str(e))
    _did_pass(False, get)


print('attempting to create a quote with a duplicate name, expecting error')
req = requests.post(url=url,
                    data=json.dumps({"name": "big sale",
                                     "price": 4223.12,
                                     "items": ['mercedes', 'audi', 'bmw']}),
                    headers=headers)
_did_pass(299 < req.status_code, req)



print('attempting to create a quote with negative, expecting error')
req = requests.post(url=url,
                    data=json.dumps({"name": "fail me",
                                     "price": -4223.12,
                                     "items": ['mercedes', 'audi', 'bmw', 'porsche']}),
                    headers=headers)
_did_pass(299 < req.status_code, req)

print('attempting to create a quote with null-like name')
req = requests.post(url=url,
                    data=json.dumps({"name": "",
                                     "price": 4223.12,
                                     "items": ['mercedes', 'audi', 'bmw', 'porsche']}),
                    headers=headers)
_did_pass(299 < req.status_code, req)

print('attempting to update a nonexistent quote')
req = requests.patch(url=url,
                     data=json.dumps({"name": "not real",
                                      "items": ['mercedes', 'audi', 'volkswagen', 'porsche']}),
                     headers=headers)
_did_pass(299 < req.status_code, req)


print('attempting to update the items and the price simultaneously')
req = requests.patch(url=url,
                     data=json.dumps({"name": "big sale",
                                      "price": 56318.39,
                                      "items": ['audi', 'volkswagen', 'porsche', 'bmw', 'opel']}),
                     headers=headers)
_did_pass(210 > req.status_code >= 200, req)

print('sending GET to check if bulk update worked')
get = requests.get(url)

try:
    big_sale = [quote for quote in get.json() if 'name' in quote.keys()][0]
    items = [item['name'] for item in big_sale['items']]
    are_items_in = [item in items for item in ['audi', 'volkswagen', 'porsche', 'bmw', 'opel']]
    _did_pass(get.status_code == 200 and big_sale['price'] == '56318.39' and len(items) == 5 and all(are_items_in), get)
except Exception as e:
    print(str(e))
    _did_pass(False, get)


print('attempting to delete non-existent quote')
req = requests.delete(url=url,
                      data=json.dumps({"name": "not real"}),
                      headers=headers)
_did_pass(299 < req.status_code, req)


print('attempting to delete the quote')
req = requests.delete(url=url,
                      data=json.dumps({"name": "big sale"}),
                      headers=headers)
_did_pass(210 > req.status_code >= 200, req)


print('sending GET to check presence ')
get = requests.get(url)
big_sale = [quote for quote in get.json() if 'name' in quote.keys()]
_did_pass(get.status_code == 200 and big_sale == [])


print('Success! All good requests succeeded, all deliberately bad requests failed!')
