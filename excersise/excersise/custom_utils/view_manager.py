from django.db import transaction
from django.db import models
from rest_framework.request import Request
from rest_framework.response import Response
import logging
import json
from django.core.exceptions import ValidationError
from rest_framework import mixins, generics

logger = logging.getLogger(__name__)


class APIView(mixins.ListModelMixin,
              mixins.CreateModelMixin,
              mixins.UpdateModelMixin,
              mixins.DestroyModelMixin,
              generics.GenericAPIView):

    model_logger = None
    OPERATIONS_MAP = {
        'POST': 'CREATE',
        'PATCH': 'UPDATE',
        'DELETE': 'DELETE'
    }


    def initial(self, request, *args, **kwargs):
        if request.method in self.OPERATIONS_MAP.keys():
            self.model_logger.operation = self.OPERATIONS_MAP[self.request.method]
            setattr(self.model_logger, self.queryset.model.__name__, self.get_object())
            self.model_logger.save()
        super(APIView, self).initial(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        try:
            return super(APIView, self).dispatch(request, *args, **kwargs)
        except ValidationError as e:
            error = {'errorCode': str(e.code), "description": str(e.message), "level": "error"}
            logger.error(json.dumps(error))
            response = Response(data=error, status=409)
            return self.finalize_response(request, response, *args, **kwargs)

