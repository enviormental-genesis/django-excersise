from django.db import models
from django.db.models import UniqueConstraint, Q, F
from excersise.custom_utils.model_manager import Model
from api.special_fields import *
from django.utils.translation import gettext_lazy as _

class Item(Model):
    name = models.CharField(max_length=255)


class Quote(Model):
    name = models.CharField(max_length=255, null=False)
    price = PriceField(max_digits=12)
    items = models.ManyToManyField(Item)

    class Meta:
        constraints = [UniqueConstraint(fields=['name'], condition=Q(deleted_at__isnull=True),
                                        name='unique_and_active')]



class QuoteLog(Model):
    class OperationChoices(models.TextChoices):
        CREATE = 'CREATE', _('Create')
        UPDATE = 'UPDATE', _('Update')
        DELETE = 'DELETE', _('Delete')

    created_date = models.DateTimeField(auto_now=True)
    quote = models.ForeignKey(Quote, on_delete=models.SET_NULL, null=True)
    operation = models.CharField(choices=OperationChoices.choices, max_length=12)

    class Meta:
        db_table = "api_quote_log"

