from api.models import Item, Quote
from rest_framework import serializers
from django.conf import settings
import json

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['id', 'name']

class QuoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quote
        depth = 2
        exclude = ('deleted_at', )
