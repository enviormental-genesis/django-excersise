from django.db.models import DecimalField
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _



class PriceField(DecimalField):

    def __init__(self, *args, decimal_places=2, **kwargs):
        super(PriceField, self).__init__(*args, decimal_places=decimal_places, **kwargs)

    def to_python(self, value):
        value = super(PriceField, self).to_python(value)
        if value < 0.01:
            raise ValidationError(_('Price must be a positive value'), code='3')
        return value

