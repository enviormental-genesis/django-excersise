from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins, generics
from api.models import Item, Quote, QuoteLog
from api.serializers import ItemSerializer, QuoteSerializer
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import logging
import json



logger = logging.getLogger('django')

OPERATIONS_MAP ={
    'POST': QuoteLog.OperationChoices.CREATE,
    'PATCH': QuoteLog.OperationChoices.UPDATE,
    'DELETE': QuoteLog.OperationChoices.DELETE
}

class QuoteView(mixins.ListModelMixin,
                mixins.CreateModelMixin,
                mixins.DestroyModelMixin,
                mixins.UpdateModelMixin,
                generics.GenericAPIView):

    queryset = Quote.objects.all()
    serializer_class = QuoteSerializer
    quote_log = QuoteLog()

    def get_object(self):
        quote = self.queryset.filter(name=self.request.data['name']).first()
        self.check_object_permissions(self.request, quote)
        return quote

    def initial(self, request, *args, **kwargs):
        if request.method in OPERATIONS_MAP.keys():
            self.quote_log.operation = OPERATIONS_MAP[self.request.method]
            self.quote_log.quote = Quote.objects.filter(name=self.request.data.get('name')).first()
            self.quote_log.save()
        super(QuoteView, self).initial(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        try:
            return super(QuoteView, self).dispatch(request, *args, **kwargs)
        except ValidationError as e:
            error = {'errorCode': str(e.code), "description": str(e.message), "level": "error"}
            logger.error(json.dumps(error))
            response = Response(data=error, status=409)
            return self.finalize_response(request, response, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = self.list(request, *args, **kwargs).data
        for quote in data:
            quote['items'] = [{key:item[key] for key in item.keys() if key in ItemSerializer.Meta.fields} for item in quote['items']]
        return Response(data)

    def post(self, request, *args, **kwargs):
        if not request.data.get('name'):
            raise ValidationError(_('Quote needs a name'), code='2')
        if self.get_object():
            raise ValidationError(_('Quote already exists'), code='1')

        response = self.create(request, *args, **kwargs)
        items = [Item.objects.get_or_create(name=item)[0] for item in request.data['items']]
        quote = self.queryset.filter(name=request.data['name']).first()
        quote.items.set(items, clear=True)
        return response

    def patch(self, request, *args, **kwargs):
        response = self.update(request, *args, **kwargs)
        items = [Item.objects.get_or_create(name=item)[0] for item in request.data['items']]
        quote = self.queryset.filter(name=request.data['name']).first()
        quote.items.set(items, clear=True)
        return response

    def delete(self, request, *args, **kwargs):
        if not self.get_object():
            raise ValidationError(_('This quote does not exist'), code='4')
        return self.destroy(request, *args, **kwargs)
