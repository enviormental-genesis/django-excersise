FROM python:3.9-alpine

# set work directory
WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

COPY ./excersise ./

RUN chmod 777 ./entrypoint.sh

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
